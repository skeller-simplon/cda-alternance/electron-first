// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
const { ipcRenderer } = require("electron");

ipcRenderer.on("cpu", (event, data) => {
  document.querySelector("#cpu").textContent = data.toFixed(2);
});

ipcRenderer.on("memory", (event, data) => {
  document.querySelector("#memory").innerHTML = data.toFixed(2);
});

ipcRenderer.on("cpuCount", (event, data) => {
  document.querySelector("#cpuCount").innerHTML = data;
});
